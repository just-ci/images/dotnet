ARG DOTNET_VERSION=6.0

FROM mcr.microsoft.com/dotnet/sdk:${DOTNET_VERSION} AS builder
ARG REPORT_GENERATOR_VERSION=5.1.9
RUN dotnet tool install --global dotnet-reportgenerator-globaltool --version=${REPORT_GENERATOR_VERSION}

FROM mcr.microsoft.com/dotnet/runtime:${DOTNET_VERSION}
COPY --from=builder /root/.dotnet/tools/ /opt/bin
ENV PATH="/opt/bin:${PATH}"
